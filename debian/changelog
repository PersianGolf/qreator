qreator (20.02.1-0ubuntu1) focal; urgency=medium

  * New upstream release
  * Port to Python 3. Thanks to Balló György
    (closes: LP: #1824551)
  * Fix for failure to list WiFI networks when
    sitting next to accesspoint. Thanks to
    Chow Loong Jin (closes: LP: #1763961)

 -- David Planella <david.planella@ubuntu.com>  Sun, 09 Feb 2020 23:06:50 +0100

qreator (16.06.2-0ubuntu1) xenial; urgency=low

  * Adapt to new PIL namespace to fix crash (closes: #1771122)
  * Deprecate libnm-glib interface (closes: #1763958).
  * Thanks Lubomir Rintel

 -- David Planella <david.planella@ubuntu.com>  Mon, 30 Jul 2018 01:49:26 +0200

qreator (16.06.1-0ubuntu1) xenial; urgency=low

  * New upstream release
  * Fix inability to create QR codes in Ubuntu 16.04
    Thanks to Felix Eckhofer! (closes: #1573577)
  * Packaging fixes. Thanks to Didier Roche
    (closes: #1210408)
  * Fix missing edit mode icons. Thanks to Stefan
    Schwarzburg (closes: #1182766)
  * Add geoclue-ubuntu-geoip dependency (closes: #1186224)
  * Provide a mechanism to disable QR code plugins
    Thanks to Stefan Schwarzburg (closes: #1181723)
  * Depend on gnome-icon-theme-symbolic for Xubuntu
    (closes: #1187706)
  * Allow semicolon in wifi password. Thanks Stefan
    Schwarzburg (closes: #1208033)
  * Fix for "No module named linuxmint". Thanks Stefan
    Schwarzburg (closes: #1179176)

 -- David Planella <david.planella@ubuntu.com>  Mon, 13 Jun 2016 21:17:47 +0200

qreator (13.05.3-0ubuntu1) raring; urgency=low

  * New upstream release
  * Bugfix for Qreator won't start when called with LC_ALL=C
    (closes: #1173584)
  * Updated translations
  * Now properly fixed package version to be non-native

 -- David Planella <david.planella@ubuntu.com>  Sun, 19 May 2013 10:47:52 +0200

qreator (13.05.2-0ubuntu1) raring; urgency=low

  * Fixed package version to be non-native, and watch file regexp

 -- David Planella <david.planella@ubuntu.com>  Sun, 19 May 2013 09:54:49 +0200

qreator (13.05.2ubuntu1) raring; urgency=low

  * New upstream release
  * Fix lp:1173709 Segfault when shortening tinyurl.com URL
  * Fix lp:1178146 Unity quicklist entries missing some QR code types
  * Updated translations

 -- David Planella <david.planella@ubuntu.com>  Sat, 18 May 2013 13:58:50 +0200

qreator (13.05.1) raring; urgency=low

  * Fix lp:1173057, whereby protocol removal in the URL QR code lead to
    removing unwanted characters
  * Launchpad automatic translations update.
  * Preparation for the Qreator 13.04 release.      It includes:      -
    Fix for lp:1173306 (thus it's dependent on the branch with the fix
    to land)   - Version and copyright bump   - Fixed toolbar logic
  * Fixes lp:1173306 whereby the GTK version in Ubuntu 13.04 does not
    accept None as an argument to set the draggable sources list.
    We're already explicitly setting URI draggable sources, so we can
    remove the function to initially set the sources list.
  * add a sorting feature to the drop down menu of the Wifi QR Code
    datatype
  * merge the fixes_1081538 branch to ensure that Wifis are shown in the
    drop down menu
  * basic setup to start with tests: removed About dialog tests since we
    don't use that, switched on code analysis when running 'quickly
    test'
  * small change in the icon loading to allow icon files of any size and
    still get the correct size for the interface
  * update to latest trunk
  * updated pot files
  * fix bug 1086937; replace isdg with isgd
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * fixes bug 1081535 ; the signal handler was missing in the ui file
  * merged with branch to fix bug 1082705
  * fixes bug 1083570 by including python-requests as a dependency
  * updated pot files
  * removed translatable description in abstract class QRCodeType;
    removed pylint errors and warnings
  * merged basic printing support
  * Merged i18n fixes and updated .pot file
  * Bumped version number to allow installing the daily PPA over a
    stable version. Some additional whitespace changes
  * Updated POT file for translators, changed a translatable text to be
    more readable
  * Bumped version number in the Debian changelog to make the automatic
    builds succeed
  * added use_agent=True in order to add additional databases
  * increased pep8 and pylint compatibility
  * added missing on_prepared callback to QRCodeSMSGtk.py
  * moved the cache recreation in a separate thread to really keep the
    ui feel snappy
  * reverted back to just recreating the cache for now
  * minor bug
  * after recreating cache, use it as well
  * recreate the icon and appname cache everytime qreator is launched
  * merged with latest trunk
  * now really using cache in XDG_CACHE_HOME
  * Sprite saving location moved to $XDG_CACHE_HOME/qreator/.
  * The modification shifts the loading time of the app-icons into the
    first software start.    At the first start of the software, a
    sprite map is created, which stores **all** icons in a single large
    file, and the appnames in another text file.       This increases
    the loading time (all values measured on my maschine) on the first
    start from 3.7sec to 4.5sec. But future calls will decrease the time
    to 0.2sec.    So we got a factor of 18 in speed!
  * Looking through the software-center revealed some interesting
    possibility to speedup the loading time:   Directly getting the icon
    from the database instead of creating a softwarecenter-appdetails
    object   brings the time to load the apps from 2sec to 0.2sec.
    I think a factor of 10 is a good start to make the autocompletion
    loading smoother :-)
  * A first attempt to introduce threads in order to give IO-heavy
    autocompleters a chance to do the lifting in the background.
    Since getting the actual SoftwareCenterApp apps seems to be
    impossible with python-threading or with python-multiprocessing,
    this does not make it much faster. But it will keep the Window from
    freezing.
  * Merged a fix to make eog work with drag and drop, from lp:~stefan-
    schwarzburg/qreator/drag_n_drop_support
  * Merged drag and drop support from lp:~stefan-
    schwarzburg/qreator/drag_n_drop_support
  * Launchpad automatic translations update.
  * Merged VCARD QR code support from lp:stefan-
    schwarzburg/qreator/VCARD_support, with a bunch of UI changes
  * Merged fixes from lp:~stefan-
    schwarzburg/qreator/fix_bug_1006635_selection
  * Merged lp:~stefan-schwarzburg/qreator/qreator:      * Fixes
    lp:1006630 'Resize QR input widgets'   * Additional changes to merge
    proposal:     * Added Stefan Schwarzburg to autors credits     *
    Added margins to qr_input box     * Disabled automatic resizing of
    QR code
  * Merged translations
  * An attempt to get notebook pages to resize. Thanks to xubuntix for
    contributing to the code
  * Added clear icons to text entries
  * Fixed focus issue to show placeholder text while loading the
    Software Center database
  * Merged translations from Launchpad
  * Decoupled QR code types from the QR window and from a particular GUI
    toolkit (right now only GTK is supported)
  * Added icons for the 'clear' action to all text entries, used
    symbolic icons in the QR page, delayed loading Software Centre apps
    until the apps tab is loaded for the first time
  * Make optparse not crash when using gettext, thanks Nicolas Delvaux
    for the help
  * Updated version to follow that of the package

 -- David Planella <david.planella@ubuntu.com>  Wed, 08 May 2013 20:21:47 +0200

qreator (13.05) raring; urgency=low

  * Added new contributor, bumped version number

 -- David Planella <david.planella@ubuntu.com>  Wed, 08 May 2013 20:21:46 +0200

qreator (12.11.1ubuntu1) raring; urgency=low

  * Bumped version number.

 -- David Planella <david.planella@ubuntu.com>  Fri, 23 Nov 2012 09:26:33 +0100

qreator (12.05.6) precise; urgency=low

  * Committed Debian packaging in preparation for daily builds. Updated
    .pot file
  * Reverted extras changes in the desktop file
  * Merged new features and extras packaging from the lp:app-review-
    packaging branch
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * Launchpad automatic translations update.
  * Fixed a couple of Catalan translations
  * Revamped the About dialog, updated POT file
  * Added dependency for NetworkManager's typelib
  * Removed unnecessary help files, marked files for translation,
    refreshed POT template, updated Catalan translation
  * Updated ignores
  * Merged new translations
  * Added support for showing autodetected SSIDs. Moved contributors to
    About page instead of AUTHORS file, as otherwise Quickly adds them
    to all source files - will need to investigate what the proper way
    to credit contributors and assign copyright is

 -- David Planella <david.planella@ubuntu.com>  Mon, 28 May 2012 13:16:10 +0200

qreator (12.05.4) precise; urgency=low

  * Initial release to extras.ubuntu.com.

 -- David Planella <david.planella@ubuntu.com>  Mon, 28 May 2012 12:51:06 +0200
