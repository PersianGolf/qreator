# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gi
gi.require_version('GtkChamplain', '0.12')
from gi.repository import Gtk, GtkChamplain, Clutter, Champlain
from qreator_lib.helpers import get_data_file
gi.require_version('GtkClutter', '1.0')
from gi.repository import GtkClutter


class QRCodeLocationGtk(object):
    def __init__(self, qr_code_update_func):
        self.qr_code_update_func = qr_code_update_func
        self.builder = Gtk.Builder()

        self.builder.add_from_file(
            get_data_file('ui', '%s.ui' % ('QrCodeLocation',)))
        self.grid = self.builder.get_object('qr_code_location')

        GtkClutter.init([])
        map_widget = GtkChamplain.Embed()
        map_widget.set_hexpand(True)
        map_widget.set_vexpand(True)

        map_grid = self.grid

        #map_grid.set_size_request(-1, 350)
        map_grid.attach(map_widget, 0, 0, 1, 1)

        self.map_view = map_widget.get_view()
        self.map_view.set_reactive(True)
        map_widget.connect('button-release-event',
                           self.on_map_widget_button_press_event,
                           self.map_view)

        # Get the current location, center the map on it, and initialize
        # other map features
        self.get_current_location()
        self.map_view.set_zoom_level(1)
        self.map_view.set_kinetic_mode(True)

        scale = Champlain.Scale()
        scale.connect_view(self.map_view)
        self.map_view.bin_layout_add(scale, Clutter.BinAlignment.START,
                                     Clutter.BinAlignment.END)

        self.grid.show_all()

    def on_activated(self):
        pass

    def on_prepared(self):
        pass

    def on_map_widget_button_press_event(self, actor, event, view):
        x, y = event.get_coords()
        lon, lat = view.x_to_longitude(x), view.y_to_latitude(y)

        self.builder.get_object('lat_entry').set_text(str(lat))
        self.builder.get_object('lon_entry').set_text(str(lon))

        self.qr_code_update_func('geo:{0},{1}'.format(str(lat), str(lon)))

        return True

    def get_current_location(self):
        '''Gets the current location from geolocation via using Geoclue-2.0'''

        gi.require_version('Geoclue', '2.0')
        from gi.repository import Geoclue

        Geoclue.Simple.new('qreator',
                           Geoclue.AccuracyLevel.EXACT,
                           callback=on_current_location_ready,
                           user_data=self)
        return True


def on_current_location_ready(self, widget, data=None):
    location = self.get_location()
    latitude = location.get_property('latitude')
    longitude = location.get_property('longitude')
    if latitude != 0 and longitude != 0:
        data.builder.get_object('lat_entry').set_text(str(latitude))
        data.builder.get_object('lon_entry').set_text(str(longitude))
        data.map_view.center_on(latitude, longitude)
        data.map_view.set_zoom_level(15)
    return
