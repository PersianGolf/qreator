# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from qreator_lib.helpers import get_media_file


class DataformatsMount(type):
    """ http://martyalchin.com/2008/jan/10/simple-plugin-framework/"""
    def __init__(cls, name, bases, attrs):  # pylint: disable=W0231,W0613
        if not hasattr(cls, 'dataformats'):
            # This branch only executes when processing the mount
            # point itself.  So, since this is a new dataformat type,
            # not an implementation, this class shouldn't be
            # registered as a plugin. Instead, it sets up a list where
            # dataformats can be registered later.
            cls.dataformats = []
        else:
            # This must be a dataformat implementation, which should
            # be registered.  Simply appending it to the list is all
            # that's needed to keep track of it later.
            cls.dataformats.append(cls)


class QRCodeType(object, metaclass=DataformatsMount):
    """This class is the base for QRCode dataformats. It is not used itself.

    Classes implementing a dataformat type should provide the following class
    variables:
    ====================  =====================================================
    description           translateble, very short, used in the GUI
    cmdline_option        make this unique in Qreator, since it will be used to
    icon_name             the name of the icon in <qreator>/data/media/, which
                          should be used for this datatype
    ====================  =====================================================

    Classes implementing a dataformat type should also provide the following
    methods:
    ====================  =====================================================
    create_widget         This method is called during init to create the
                          widget for this datatype.
    create_code           The paramters are data format specific, but the
                          method should be able to recreate a code from saved
                          history data.
                          This method should return a single string to pass
                          to the general code update function.
    ====================  =====================================================
    """

    # replace the description, and make it translatable with _("")
    description = "DataFormat"
    icon_name = "dataformat.png"  # replace this in subclass
    cmdline_option = "--dataformat"  # replace this in subclass

    def __init__(self, qr_code_update_func):
        self.qr_code_update_func = qr_code_update_func
        self.icon_path = get_media_file(self.icon_name)
        self.create_widget()  # pylint: disable=E1101
