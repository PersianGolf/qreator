# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from qreator_lib.i18n import _
from .QRCodeType import QRCodeType
from .QRCodeWifiGtk import QRCodeWifiGtk

class QRCodeWifi(QRCodeType):
    description = _('Wifi network')
    icon_name = 'wifi.png'
    cmdline_option_long = "--wifi"
    cmdline_option_short = "-w"
    message = _("Create a QR code for WiFi settings")

    def create_widget(self):
        self.widget = QRCodeWifiGtk(self.qr_code_update_func)
